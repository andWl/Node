var multiparty = require('multiparty');
var http = require('http');
var util = require('util');
var count = 0;
var form = new multiparty.Form();




///////////////////
http.createServer(function(req, res) {












  if (req.url === '/upload' && req.method === 'POST') {
    // parse a file upload
/////////////////////////////////////////////////////////////////////////////
      var count = 0;

    // Errors may be emitted
    // Note that if you are listening to 'part' events, the same error may be
    // emitted from the `form` and the `part`.
    form.on('error', function(err) {
      console.log('Error parsing form: ' + err.stack);
    });

    // Parts are emitted when parsing the form
    form.on('part', function(part) {
      // You *must* act on the part by reading it
      // NOTE: if you want to ignore it, just call "part.resume()"

      if (!part.filename) {
        // filename is not defined when this is a field and not a file
        console.log('got field named ' + part.name);
        // ignore field's content
        part.resume();
      }

      if (part.filename) {
        // filename is defined when this is a file
        count++;
        console.log('got file named ' + part.name);
        // ignore file's content here
        part.resume();
      }

      part.on('error', function(err) {
        // decide what to do
      });
    });

    // Close emitted after form parsed
    form.on('close', function() {
      console.log('Upload completed!');
      // res.setHeader('content-type','text/plain');
            res.writeHead(200, {'content-type': 'text/plain'});
      res.end('Received ' + count + ' files');
    });

    // Parse req
    form.parse(req);
    ////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////
    // form.parse(req, function(err, fields, files) {
    //   Object.keys(fields).forEach(function(name) {
    //     console.log('got field named ' + name);
    //   });
    //
    //   Object.keys(files).forEach(function(name) {
    //     console.log('got file named ' + name);
    //   });
    //
    //   console.log('Upload completed!');
    //       // res.setHeader('content-type','text/plain');
    //
    //             res.writeHead(200, {'content-type': 'text/plain'});
    //   res.end('Received ' + files.length + ' files');
    // });
/////////////////////////////////////////////////////////////////////////////////



    form.parse(req, function(err, fields, files) {
      Object.keys(fields).forEach(function(name) {
        console.log('got field named ' + name);
      });

      Object.keys(files).forEach(function(name) {
        console.log('got file named ' + name);
      });

      console.log('Upload completed!');
          // res.setHeader('content-type','text/plain');

                res.writeHead(200, {'content-type': 'text/plain'});
      // res.end('Received ' + files.length + ' files');


      // res.writeHead(200, {'content-type': 'text/plain'});
      // res.write('received upload:\n\n');
      res.end('received upload:\n\n' + util.inspect({fields: fields, files: files}));
    });

    return;
  }

  // show a file upload form
  res.writeHead(200, {'content-type': 'text/html'});
  res.end(
    '<form action="/upload" enctype="multipart/form-data" method="post">'+
    '<input type="text" name="title"><br>'+
    '<input type="file" name="upload" multiple="multiple"><br>'+
    '<input type="submit" value="Upload">'+
    '</form>'
  );
}).listen(3000);

//
//
//
//
// var multiparty = require('multiparty');
// var http = require('http');
// var util = require('util');
//
// http.createServer(function(req, res) {
//   if (req.url === '/upload' && req.method === 'POST') {
//     // parse a file upload
//     var form = new multiparty.Form();
//
//     form.parse(req, function(err, fields, files) {
//
//       console.log("files!!! :" );
//       console.log(files );
//       res.writeHead(200, {'content-type': 'text/plain'});
//       res.write('received upload:\n\n');
//       res.end(util.inspect({fields: fields, files: files}));
//     });
//
//     return;
//   }
//
//   // show a file upload form
//   res.writeHead(200, {'content-type': 'text/html'});
//   res.end(
//     '<form action="/upload" enctype="multipart/form-data" method="post">'+
//     '<input type="text" name="title"><br>'+
//     '<input type="file" name="upload" multiple="multiple"><br>'+
//     '<input type="submit" value="Upload">'+
//     '</form>'
//   );
// }).listen(3000);
