// const http = require('http');
//
// const hostname = 'localhost';
// const hostname = '192.168.32.141';
//
//
// const port = 3000;
//
// const server = http.createServer((req, res) => {
//   // res.statusCode = 200;
//   // res.setHeader('Content-Type', 'text/plain');
//     res.writeHead(200, {'Content-Type': 'text/plain'});
//   res.end('Hello World\n');
// });
//
// server.listen(port, hostname, () => {
//   console.log(`Server running at http://${hostname}:${port}/`);
// });

//
//
// var http = require('http');
// var callbackFunction = function(req, res){
//   res.writeHead(200, {'Content-Type': 'text/plain'});
//   res.end('Hello World\n');
// };
//
// http.createServer(callbackFunction).listen(3000);
//  console.log('Server running at http://localhost:3000/');



const http = require('http');

const hostname = '127.0.0.1';
const port = 1337;

http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end('Hello World\n');
}).listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
