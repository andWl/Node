var express = require('express');
var bodyParser = require('body-parser');
var multer = require('multer');


var multiparty = require('multiparty');
var http = require('http');
var util = require('util');

//_storage 변수에 diskStorage 함수를 호출하면 _staroage의 객체를 리턴한다.
var _storage = multer.diskStorage({
  //destination 함수에 대한 약속
  //cb는 콜백함수 , 두번째 인자로 destination(uploads), filename(file.originalname) 결정한다.
  destination: function (req, file, cb) {
    cb(null, 'uploads/') // uploads 폴더에 저장한다.
  },
  //filename 함수에 대한 약속
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
})

//multer라는 함수에다가 옵션을 줘서 설정을 시키면
//이함수는 upload 를 받아낼수 있는 미들웨어라는것을 return 해준다.
//그러면 우리는 upload를 통해서 여러가지 제어작업을 할 수 있다.
//_storage 변수 위에 선언 되어있다. 그 위치로 저장
//storage는 property의 이름, _storage는 그값이다.
var upload = multer({ storage: _storage })

var fs = require('fs');
var app = express();

app.use(bodyParser.json({limit: ' 50mb'}));

app.use(bodyParser.urlencoded({ extended: false }));
app.locals.pretty = true;


//여기서 http://192.168.32.142:3000/user/api.zip 하면 파일 받아진다.
app.use('/user', express.static('uploads'));
app.set('views', './views_file');
app.set('view engine', 'jade');

// 여기 upload가 jade랑 같은 이름이 아니더라도 상관없이 올라간다.
//ex ) uploadd 로 지정하면 http://192.168.32.142:3000/uploadd 로했을때 file올라감
app.get('/upload', function(req, res){
  res.render('upload');
});

//post로 전송한걸 받으려면 라우터를 post로 업로드 해주면 된다.
//upload.single('userfile') 이 뜻은 (upload는 multer를 통해 만든 모듈(미들웨어)
//사용자가 전송한 데이터에서 파일이 포함되어있다면 파일을 가공해서
//requst 객체의 파일 이라는 프로퍼티를 암시적으로 추가하도록 약속 되어있는 함수다.
//사용자가 post방식으로 전송한 데이터가 /upload 라는
//디렉터리로 향하고 있다면 /upload 이것을 충족시키기 때문에 funtion이 실행된다.
//funtion이 실행 되기 전에 upload.single 이 먼저 실행이 된다.
//req 객체안에 파일이라는 프로퍼티가 포함되어있다는것.

//userfile은 upload.jade에서 file 보낼때 name이다
app.post('/upload', upload.single('userfile'), function(req, res){
//메시지 출력 , req.file 만 출력하면 object Object 이런식으로 뜬다. 즉 객체라는거 !
//req라는 객체에 file이라는 property에 사용자가 전송한 파일에대한 정보를 넣어준거다

console.log(req.file);

//화면에 출력
  res.send('Uploaded : '+req.file.filename);
});
// app.get('/topic/new', function(req, res){
//   fs.readdir
//   ('data', function(err, files){
//     if(err){
//       console.log(err);
//       res.status(500).send('Internal Server Error');
//     }
//     res.render('new', {topics:files});
//   });
// });
// app.get(['/topic', '/topic/:id'], function(req, res){
//   fs.readdir('data', function(err, files){
//     if(err){
//       console.log(err);
//       res.status(500).send('Internal Server Error');
//     }
//     var id = req.params.id;
//     if(id){
//       // id값이 있을 때
//       fs.readFile('data/'+id, 'utf8', function(err, data){
//         if(err){
//           console.log(err);
//           res.status(500).send('Internal Server Error');
//         }
//         res.render('view', {topics:files, title:id, description:data});
//       })
//     } else {
//       // id 값이 없을 때
//       res.render('view', {topics:files, title:'Welcome', description:'Hello, JavaScript for server.'});
//     }
//   })
// });


// app.post('/topic', function(req, res){
//   var title = req.body.title;
//   var description = req.body.description;
//   fs.writeFile('data/'+title, description, function(err){
//     if(err){
//       console.log(err);
//       res.status(500).send('Internal Server Error');
//     }
//     res.redirect('/topic/'+title);
//   });
// })


app.listen(3000, function(){
  console.log('Connected, 3000 port!');
})
