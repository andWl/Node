




// // 변수 및 배열 및 정렬
// console.log('hello world');
//
// var temp = 1
// console.log(temp+"\n")
//
// var li = ['a', 'b', 'c', 'd', 'e'];
// console.log("추가전길이:",li.length,"\n")
// li.push('f');
// console.log(li);
// console.log("추가후길이,",li.length,"\n")
//
// var li = ['a', 'b', 'c', 'd', 'e'];
// li=li.concat(["f","g"]);//push는 그냥 li.push("f"), concat은 대입해야되고 [] 써야됨
// console.log("concat", li,"\n");
//
// var li = ['a', 'b', 'c', 'd', 'e'];
// li.unshift('z');//z, a, b, c, d, e 가 출력// index가 하나씩 밀린다
// console.log("unshift", li,"\n");
//
// var li = ['a', 'b', 'c', 'd', 'e'];
// li.splice(2, 2, 'B')//a,b,B,e : 인덱스값이2인 원소포함 뒤로 2개 삭제 후 앞에 B를 삽입
// console.log("splice1", li,"\n");
//
// var li = ['a', 'b', 'c', 'd', 'e'];
// li.splice(2, 0, 'B','C');//,a,b,B,C,c,d,e  : 인덱스값이2인 원소포함 뒤로 0개 삭제 후 앞에 B를 삽입
// console.log("splice2", li,"\n");
//
// var li = ['a', 'b', 'c', 'd', 'e'];
// li.shift(); //b,c,d,e 앞에꺼하나 삭제
// console.log("shift",li,"\n");
//
// var li = ['a', 'b', 'c', 'd', 'e'];
// li.pop();
// console.log("pop", li,"\n");// a,b,c,d, 뒤에꺼하나 삭제
//
// var li = ['c', 'e', 'a', 'b', 'd'];//a,b,c,d,e
// li.sort();//정렬
// console.log("sort", li,"\n");
//
// var li = ['c', 'e', 'a', 'b', 'd'];//d,b,a,e,c 그냥 정렬이 아니라 거꾸로 뒤집기만한것
// li.reverse();//역순정렬
// console.log("reverse", li,"\n");
//
// var member = ['egoing', 'k8805', 'sorialgi']
// console.log(member[0]);//0 : 인덱스
// console.log(member[1]);
// console.log(member[2]);







/* parseint    */
//// 문자열을 저장하고 있는 변수를 정수로 변환한 값을 얻으려면 parseInt(),
// 실수로 변환한 값을 얻으려면 parseFloat()과 같은 함수를 사용해야 한다. 위 문제를 parseInt() 함수를 이용하여 수정한 코드는 다음과 같다.
// //Promp는 문자열로 반환하기때문

// var str1 = "10"
// var str2 = "20"
// var num1 = parseInt(str1);
// var num2 = parseInt(str2);
// console.log(typeof(str1));
// console.log(typeof(num1));
// console.log(num1 + num2);




// /*toUpperCase */
// function get_members(){
//     return ['egoing', 'k8805', 'sorialgi'];
// }
// members = get_members();

// for(i = 0; i < members.length; i++){  // members.length는 배열에 담긴 원소의 갯수를 알려준다.
  
//     console.log(members[i].toUpperCase()+"<br/>");
// // members[i].toUpperCase()는 members[i]에 담긴 문자를 대문자로 변환해준다. 이런걸 내장함수라한다
// }





// // 비교
// console.log(a);//undefined 값이 없는상황
// var a=null;
// console.log(a);// undefined .개발자가 일부러 null값 넣은것

// console.log(undefined == null);//true
// console.log(undefined === null);//false

// console.log(null == undefined);       //true
// console.log(null === undefined);      //false
// console.log(true == 1);               //true
// console.log(true === 1);              //false
// console.log(true == '1');             //true
// console.log(true === '1');            //false
 
// console.log(0 === -0);                //true
// console.log(NaN === NaN);             //false

// if(!''){//''는 false !''는 true
//     console.log('빈 문자열')//출력
// }
// if(!undefined){
//     console.log('undefined');//출력
// }
// var a;
// if(!a){
//     console.log('값이 할당되지 않은 변수');//출력
// }
// if(!null){
//     console.log('null');//출력
// }
// if(!NaN){
//     console.log('NaN');//출력
// }   







// // if else
// if(true){
//     console.log('asfsdf ');
// }

// if(false){
//     console.log('nononono');
// }//아무것도 출력하지 않는다!!


// if(false){
//     console.log(1);
// } else if(true){
//     console.log(2);
// } else if(true){
//     console.log(3);
// } else {
//     console.log(4);
// }
// //결과 2








/*continue , break  */
// for(var i = 0; i < 10; i++){ //var로 i초기화
//     if(i === 5) {
//         break;
//     }
//     console.log('coding everybody'+i+'<br />');// 5는 생략됨 
// }
 
// for(var i = 0; i < 10; i++){
//     if(i === 5) {
//         continue;
//     }
//     console.log('coding everybody'+i+'<br />'); // 5가 생략 
// }
 







//// 함수
// var numbering = function (){
//     i = 0;
//     while(i < 10){
//         console.log(i);
//         i += 1;
//     }
// }
// numbering();//이렇게도 함수 표현 가능
//
//
//
// function get_argument(arg){//arg: 매개변수, parameter, 출력!
//         return arg*1000;
//     }
//
//     console.log(get_argument(1));//1000 -> 1 :인자, argument , 입력!!
//     console.log(get_argument(2));//2000
//     console.log(get_argument(3));
//
// function get_arguments(arg1, arg2){
//     return arg1 + arg2 // return 값은 하나만 가질 수 있다
// }
//
// console.log(get_arguments(10, 20));
// console.log(get_arguments(20, 30));



//
//
// //객체
// var grades = {'egoing': 10, 'k8805': 6, 'sorialgi': 80};
// //객체 생성은 중괄호!, 객체는 배열의 인덱스 0,1,2 대신 이름넣는거
//     //egoing :key , 10: value
//     console.log(grades['egoing']);//불러오는법
//     console.log(grades['k8805']);
//     console.log(grades["sorialgi"]);//=grades['sorial'+'gi"]; 이런식으로 프로그래밍 적으로 생성가능. []방식으로 써야할때가 있음
//     console.log(grades.sorialgi);//불러오는법 이게 더 나은듯, !=grades.sorial+gi 안됨
//
//
// var grades = {};//다른법
// grades['egoing'] = 10;//
// grades['k8805'] = 6;
// grades['sorialgi'] = 80;
//
// console.log(grades['egoing']);//불러오는법
//
// console.log(grades["sorialgi"]);//=grades['sorial'+'gi"]; 이런식으로 프로그래밍 적으로 생성가능. []방식으로 써야할때가 있음
// console.log(grades.sorialgi);//불러오는법 이게 더 나은듯, !=grades.sorial+gi 안됨
//
//
// var grades = new Object();//다른법
// grades['egoing'] = 10;
// grades['k8805'] = 6;
// grades['sorialgi'] = 80;
//
// console.log(grades['egoing']);//불러오는법
//
// console.log(grades["sorialgi"]);//=grades['sorial'+'gi"]; 이런식으로 프로그래밍 적으로 생성가능. []방식으로 써야할때가 있음
// console.log(grades.sorialgi);//불러오는법 이게 더 나은듯, !=grades.sorial+gi 안됨
// console.log(grades.egoing);
//


/*for 객체 */
// var grades = {'egoing': 10, 'k8805': 6, 'sorialgi': 80};
// for(key in grades){//여기서 key는 바꿀수 있다 아무거나 이름
//     console.log("key:"+key);//egoing, k8805, sorialgi 출력 이쁘게 출력됨 들여쓰기해서
// }
//
//
// var grades = {'egoing': 10, 'k8805': 6, 'sorialgi': 80};
// for(key in grades) {//객체에서 for 문 돌릴때
//     console.log("key : "+key+" value : "+grades[key]);
//     console.log(grades[key]+1);
// }




/* 이중 객체( 객체 안에 객체랑 함수 넣은 구조 )*/
// var grades = {
//     'list': {'egoing' : 10, 'k8805' : 8, 'sorialgi' : 80},
//     'show': function () {
//         console.log("hello world"); //객체엔 함수도 저장될수있다. 자바스크립트에서는 함수도 일종의 값이다-> 함수도 변수에 저장될수 있다.
//     }
// }
// console.log(grades.list.egoing)//alert(grades['list']['egoing']); 이랑 같다 10출력
// grades.show()//grades['show'](); 이랑같다 hello world 출력 함수 불러올때는 앞에 alert 쓰면 안됨
//
//
// for(key in grades.list) {
//     console.log(grades.list[key])
// }
//

//
// /*객체지향 프로그래밍 / 객체안에 함수 밑 for문 / this  */
// var grades = {
//     //이렇게 서로 연관되어있는 데이터와 연관 되어 있는 처리를 하나의
//     // 그릇안에 모아서 그룹화 해놓은 프로그래밍 기법이 객체지향 프로그래밍
//     'list': {'egoing': 10, 'k8805': 8, 'sorialgi': 80},
//     'show': function () {
//         for (var key in this.list) {
//             console.log(key+" ", this.list[key]);//this는 약속되어있는 변수다.
//             // 자바스크립트의 정해져있는 변수이다. 즉, 이 함수가 속해있는 객체를 가리키는 변수
//             //즉 여기서는 grades를 가리킨다. this를 grades 로 바꿔도 돌아간다.
//         }
//     }
// }
// grades.show();




/* 모듈화 장점 */

// 프로그램은 작고 단순한 것에서 크고 복잡한 것으로 진화한다. 그 과정에서 코드의 재활용성을 높이고,
// 유지보수를 쉽게 할 수 있는 다양한 기법들이 사용된다. 그 중의 하나가 코드를 여러 개의 파일로 분리하는 것이다. 이를 통해서 얻을 수 있는 효과는 아래와 같다.
// ◦자주 사용되는 코드를 별도의 파일로 만들어서 필요할 때마다 재활용할 수 있다.
// ◦코드를 개선하면 이를 사용하고 있는 모든 애플리케이션의 동작이 개선된다.
// ◦코드 수정 시에 필요한 로직을 빠르게 찾을 수 있다.
// ◦필요한 로직만을 로드 해서 메모리의 낭비를 줄일 수 있다.
// ◦한번 다운로드 된 모듈은 웹 브라우저에 의해서 저장되기 때문에 동일한 로직을 로드 할 때 시간과 네트워크 트래픽을 절약 할 수 있다. (브라우저에서만 해당)



/* let , var, const */
// 유연성의 대명사 var vs 깐깐한 let, const
// JavaScript에서 ES6로 넘어오면서, let과 const 까지도 알아야하는데요.
// 특히나 요즘 나오는 예제 코드들을 보면 let과 const를 활용하고 있으므로,
// 이제는 유연한(?) var만으로는 버티기 어렵게 되었습니다.

// 아래와 같은 변수에 var를 두번 선언하도라도 가장 마지막에 선언한 변수에 담긴 값으로 에러없이 출력됩니다.
//
// var test = "123"
// var test = "2323"
// console.log(test)







// //  var , let, const 함수 범위

// 요약하먄 var보다 let이 더 좁게 사용가능 .
// 사실 var 그리고 let, const를 사용함에 있어서 가장 중요한 부분 중 하나인데요. 
// var로 선언한 변수의 범위는 함수의 범위(function scope)를 가지고 있습니다.
// 하나의 함수내에서는 적용범위가 유효하게 됩니다.

// 하지만, let과 const는 블록단위(block scope)의 범위를 가지고 있어서,
// {}로 감싸고 있는 범위내에서 유효합니다.

// 변수의 범위가 다른 것이 어떻게 영향을 미치는지 보도록 하겠습니다.
// 기존의 var를 사용하던 개념으로 생각해보면, isAdult는 true 를 console창에 표시해 줄 것 같은데요.
// (실제로도 var를 사용하면 true를 출력하는 것을 확인할 수 있습니다.)

// 결과는 아래의 이미지에서 보시다시피, false가 찍혀 있습니다.
// 이것은 위의 isAdult와 if문안의 isAdult의 범위(scope)가 서로 다르기 때문인데요.
// 두개의 isAdult가 각각 다른 범위에서 존재하고 있는 것이지요.
// // 
// 함수범위에서 광범위하게 존재하고 유연성있게 사용하는 var는,
// bug를 발생시킬 수 있다는 공격도 항상 받아왔는데요.
// 이제 let과 const를 사용하면 되겠습니다.

//
// const name= "brown"
// let age = 21;
// let isAdult = false;
//
// if (age > 18){
//     let isAdult = true
// }
//
// console.log(isAdult)
//





/*let . const 차이  */
// // 
// 먼저 유추하기 쉬운 const를 먼저 보겠습니다.
// 상수라는 영어의 constance로부터 온 것 같은 const는
// Primitive 타입인 string, number, boolean, null, undefined의 상수 선언에 사용되어집니다. 
// 한번 선언하고 다시 선언하면 에러가 난다는 것이죠.
// 상수를 선언하는데 const를 사용하고 변수를 사용하는데 let을 사용하면 되겠군요.

// 아래와 같이 한번 선언한 const변수에 다시 값을 넣어보려고 하면 에러가 발생하는 것을 볼 수 있습니다.
// 반면에 let의 경우는 아래와 같이 에러를 발생시키지 않는 것을 볼 수 있는데요.
// let에는 const와는 반대로, 변화되는 값을 대입할 수 있다는 사실을 알 수 있겠지요.

//
// const constTest = "constTest1";
// constTest = "isChangeable"
// console.log(constTest)  // 에러 발생
//
// let letTest = "letTest1";
// letTest = "isChangeable"
// console.log(letTest)  //변경 가능
//


/* const 객체의 property는 변경가능 */
// 
// 상수선언에 사용한다고 하니, 절대 변하지 않는다고 생각할 수도 있지만, 그렇지 않습니다.
// 상수로 선언한 객체변수의 property는 변할 수 있는데요. 
// 예를 들면, 아래와 같은 코드는 정상적으로 동작합니다.
// dog의 property인 name은 정상적으로 변경하는 것입니다.
// const dog = {
//     name: "Waley",
//     bread : "something"
// };
// dog.name = "testler"
//
// console.log(dog)

// // 만약에 dog 객체를 다시 변경하고자 하면 에러난다
// dog = {
//     color: "red"
// };





/***********************
 * 
 * 모듈
 * 
 * ********************** */




// Node.js에서의 모듈의 로드
// node.demo.js (로드의 주체)
// 본 수업은 Node.js를 위한 수업이 아니기 때문에 Node.js를 실행하는 방법은 다루지 않는다.
// 호스트 환경에 따라서 모듈을 로드하는 방법이 달라진다는 것을 보여주기 위한 예제일 뿐이기 때문에 동영상 수업을 참고하자.
// 모듈을 로드하는 방법은 호스트 환경에 따라서 달라진다. Node.js에서는 아래와 같은 방법으로 모듈을 로드한다.

//
// var circle = require('./circle.js');
// console.log( 'The area of a circle of radius 4 is '+ circle.area(4));
//





// 라이브러리
// 라이브러리는 모듈과 비슷한 개념이다.
//  모듈이 프로그램을 구성하는 작은 부품으로서의 로직을 의미한다면 
// 라이브러리는 자주 사용되는 로직을 재사용하기 편리하도록 
// 잘 정리한 일련의 코드들의 집합을 의미한다고 할 수 있다. 
// 프로그래밍의 세계에는 휼룡한 라이브러리가 많다. 
// 좋은 라이브러리를 선택하고 잘 사용하는 것은 프로그래밍의 핵심이라고 할 수 있다. 










/***********************
 * 
 * 함수지향
 * 
 * ********************** */


// 함수지향
// 함수지향 카테고리의 하위 수업들은 함수형 언어로서 자바스크립트의 면모를 다룬다.
// 자바스크립트의 핵심적인 도구는 함수다. 자바스크립트의 함수는 매우 강력하다.
// 함수에 대한 이해 없이는 자바스크립트를 잘 다루기 어렵다. 또한 자바스크립트에서 함수는 객체를 이해하는 데 가장 중요한 기초를 이룬다.

// 하지만 난이도가 조금 있는 내용이다. 기초수업을 이수했다면 구체적인 자바스크립트의 
// 호스트 환경에 대한 학습으로 넘어가도 된다. 함수에 대한 고급 내용을 
// 다루는 본 카테고리는 나중에 학습해도 좋다. 하지만 언젠가는 꼭 정복해야 할 부분이다. 
// 사실 그렇게 어렵지도 않다. 다만 코드를 처음 다루는 입문자라면 함수와 관련된 기능들의 
// 취지에 공감이 잘 안될지도 모르겠다. 














/***********************
 * 
 * 유효범위
 * 
 * ********************** */

// 유효범위(Scope)는 변수의 수명을 의미한다. 아래의 예제를 보자. 결과는 global이다.
// var vscope = 'global';
// function fscope(){
//     console.log(vscope);
// }
// fscope();

// 함수 밖에서 변수를 선언하면 그 변수는 전역변수가 된다. 
// 전역변수는 에플리케이션 전역에서 접근이 가능한 변수다. 
// 다시 말해서 어떤 함수 안에서도 그 변수에 접근 할 수 있다. 
// 그렇기 때문에 함수 fscope 내에서 vscope를 호출 했을 때 함수 밖에서 선언된 vscope의 값 global이 반환된 것이다. 
// 아래 예제를 보자. 결과는 '함수안 local'과 '함수밖 global'이 출력된다.
//
// var vscope = 'global';
// function fscope(){
//     var vscope = 'local';
//     console.log('함수안 '+vscope);
// }
// fscope();
// console.log('함수밖 '+vscope);







// 즉 함수 안에서 변수 vscope을 조회(4행) 했을 때 함수 내에서 선언한 
// 지역변수 vscope(3행)의 값인 local이 사용되었다. 하지만 함수 밖에서 
// vscope를 호출(7행) 했을 때는 전역변수 vscope(1행)의 값인 global이 사용된 것이다. 
// 즉 지역변수의 유효범위는 함수 안이고, 전역변수의 유효범위는 에플리케이션 전역인데, 
// 같은 이름의 지역변수와 전역변수가 동시에 정의되어 있다면 지역변수가 우선한다는 것을 알 수 있다. 
// 아래 예제를 보자. 결과는 모두 local이다.





//
//
// var vscope = 'global';
// function fscope(){
//     vscope = 'local';
//     console.log('함수안'+vscope);
// }
// fscope();
// console.log('함수밖'+vscope);



// 함수밖에서도 vscope의 값이 local인 이유는 무엇일까? 
// 그것은 함수 fscope의 지역변수를 선언할 때 var를 사용하지 않았기 때문이다. 
// var를 사용하지 않은 지역변수는 전역변수가 된다. 
// 따라서 3행은 전역변수의 값을 local로 변경하게 된 것이다. 
// var을 쓰는 것과 쓰지 않는 것의 차이를 이해해야 한다.

// 전역변수는 사용하지 않는 것이 좋다. 
// 여러가지 이유로 그 값이 변경될 수 있기 때문이다. 
// 함수 안에서 전역변수를 사용하고 있는데, 
// 누군가에 의해서 전역변수의 값이 달라졌다면 어떻게 될까? 
// 함수의 동작도 달라지게 된다. 이것은 버그의 원인이 된다. 
// 또한 함수를 다른 에플리케이션에 이식하는데도 어려움을 초래한다.
//  함수의 핵심은 로직의 재활용이라는 점을 상기하자. 
//  변수를 선언할 때는 꼭 var을 붙이는 것을 습관화해야 한다. 
//  전역변수를 사용해야 하는 경우라면 그것을 사용하는 이유를 명확히 알고 있을 때 사용하도록 하자.




// 아래 두개의 예제는 변수 i를 지역변수로 사용했을 때와
//  전역변수로 사용했을 때의 차이점을 보여준다. 
//  전역변수는 각기 다른 로직에서 사용하는 같은 이름의 변수값을 변경시켜서 의도하지 않은 문제를 발생시킨다.

// // 지역변수의 사용
// function a (){
//     var i = 0;
// }
// for(var i = 0; i < 5; i++){
//     a();
//     console.log(i);
// }


// 전역변수의 사용
// 본 예제는 무한반복을 발생시킨다. 
// function a (){
//     i = 0;
// }
// for(i = 0; i < 5; i++){
//     a();
//     console.log(i);
// }




// 불가피하게 전역변수를 사용해야 하는 경우는 하나의 객체를 
// 전역변수로 만들고 객체의 속성으로 변수를 관리하는 방법을 사용한다.

MYAPP = {}
// 여기서 calculator 는 MYAPP이라는 객체의 속성이라고 한다. 그리고 MYAPP 속성의 값에는 다시 객체가 오고
// 그 객체의 KEY값은 left는 null, right 는 null / null은 아직 값을 지정하지 않았다는 것을 개발자가 명시하는거다
//
// MYAPP.calculator = {
//     'left' : null,
//     'right' : null
// }
// MYAPP.coordinate = {
//     'left' : null,
//     'right' : null
// }
//
// MYAPP.calculator.left = 10;
// MYAPP.calculator.right = 20;
// function sum(){
//     return MYAPP.calculator.left + MYAPP.calculator.right;
// }
// console.log(sum());
//



// 전역변수를 사용하고 싶지 않다면 아래와 같이 익명함수를 호출함으로서 이러한 목적을 달성할 수 있다.
// (function(){
//     var MYAPP = {}
//     MYAPP.calculator = {
//         'left' : null,
//         'right' : null
//     }
//     MYAPP.coordinate = {
//         'left' : null,
//         'right' : null
//     }
//     MYAPP.calculator.left = 10;
//     MYAPP.calculator.right = 20;
//     function sum(){
//         return MYAPP.calculator.left + MYAPP.calculator.right;
//     }
//     console.log(sum());
// }())
// 바로 위에 () 이거는 당장 호출하겠다는 의미 즉,
// MYAPP은 함수안에 선언 된 객체 이기 때문에 지역변수가 된다.

// 아래도 위에랑 마찬가지다. 

// function myappfn(){
//     var MYAPP = {}
//     MYAPP.calculator = {
//         'left' : null,
//         'right' : null
//     }
//     MYAPP.coordinate = {
//         'left' : null,
//         'right' : null
//     }
//     MYAPP.calculator.left = 10;
//     MYAPP.calculator.right = 20;
//     function sum(){
//         return MYAPP.calculator.left + MYAPP.calculator.right;
//     }
//     console.log(sum());
// }

// myappfn();






// 자바스크립트는 함수에 대한 유효범위만을 제공한다. 많은 언어들이 
// 블록(대체로 {,})에 대한 유효범위를 제공하는 것과 다른 점이다. 아래 예제의 결과는 coding everybody이다.

// for(var i = 0; i < 1; i++){
//     var name = 'coding everybody';
// }
// console.log(name);


// 아래는 자바 코드다 
// 자바에서는 아래의 코드는 허용되지 않는다. 
// name은 지역변수로 for 문 안에서 선언 되었는데 이를 for문 밖에서 호출하고 있기 때문이다.

// for(int i = 0; i < 10; i++){
//     String name = "egoing";
// }
// System.out.println(name);





// 자바스크립트는 함수가 선언된 시점에서의 유효범위를 갖는다. 
// 이러한 유효범위의 방식을 정적 유효범위(static scoping), 혹은 렉시컬(lexical scoping)이라고 한다. 
// var i = 5;
 
// function a(){
//     var i = 10;
//     b();
// }
 
// function b(){
//     console.log(i);
// }
 
// a();









/***********************
 * 
 * 값으로서의 함수
 * 
 * ********************** */


// JavaScript에서는 함수도 객체다. 다시 말해서 일종의 값이다. 
// 예를 들어서 var a="value"
// 여기서 value 는 값이기 때문에 변수에 담을수 있다. 즉, 함수도 값이기 때문에 변수에 담을 수 있다.

// 거의 모든 언어가 함수를 가지고 있다. JavaScript의 함수가 다른 언어의 함수와
// 다른 점은 함수가 값이 될 수 있다는 점이다. 다음 예제를 통해서 그 의미를 알아보자.

// function a(){}

// 이놈을 번역하면 
// var a= function(){} 이 된다. 


// 위의 예제에서 함수 a는 변수 a에 담겨진 값이다. 함수는 변수에만 담기는게 아니고
// 또한 함수는 객체의 값으로 포함될 수 있다. 여기서 객체의 key는 b이고 value는 funtion() 이다. 
// 즉, b는 key / function() 은 value가 된다. 
// 즉 여기서 key는 변수와 같은 역할을 한다는 것이고, 객체 안에서 변수의 역할을 하는 것을 "속성(property)" 이라고 한다.
// 그리고 그 속성에 저장되어 있는 값이 함수라면 그 함수는 메소드(method)라고 부른다.
// 즉 a라는 변수안에 담긴 객체 안에는 b라는 속성이 있는데 b의 value는 함수이기 때문에 method다. method b 라고 부른다. 
// a = {
//     b:function(){
//     }
// };


// 함수는 값이기 때문에 다른 함수의 인자로 전달 될수도 있다. 아래 예제를 보자.
//
// function cal(func_test, num){
//     return func_test(num)
// }
// function increase(num){
//     return num+1
// }
// function decrease(num){
//     return num-1
// }
// console.log(cal(increase, 1));
// console.log(cal(decrease, 1));


// 10행을 실행하면 함수 increase와 값 1이 함수 cal의 인자로 전달된다. 
// 함수 cal은 첫번째 인자로 전달된 increase를 실행하는데 이 때 두번째 인자의 값이 1을 인자로 전달한다. 
// 함수 increase은 계산된 결과를 리턴하고 cal은 다시 그 값을 리턴한다.







// 함수는 함수의 리턴 값으로도 사용할 수 있다.
// 여기서 funcs 라는 객체에 plus속성(key)에 function(value) 메소드가 담겨져 있다..

//
// function cal(mode){
//     var funcs = {
//         'plus' : function(left, right){return left + right},
//         'minus' : function(left, right){return left - right}
//     }
//     //mode 가 key 값을 의미하고 return 되는건 value인 function 즉 메소드가 리턴된다.
//     return funcs[mode];
// }
//
//
//
// // 괄호가 나왔다는건 함수가 호출된다는걸의미!!
// console.log(cal('plus')(2,1));
// console.log(cal('minus')(2,1));




// 당연히 배열의 값으로도 사용할 수 있다.
// 10행을 실행하면 함수 increase와 값 1이 함수 cal의 인자로 전달된다.
//  함수 cal은 첫번째 인자로 전달된 increase를 실행하는데 이 때 두번째 인자의 값이 1을 인자로 전달한다. 
// 함수 increase은 계산된 결과를 리턴하고 call은 다시 그 값을 리턴한다.
// var process = [
//     function(input){ return input + 10;},
//     function(input){ return input * input;},
//     function(input){ return input / 2;}
// ];

// console.log(process)
// console.log(process.length)

// var input = 1;
// for(var i = 0; i < process.length; i++){
//     input = process[i](input);
// }
// console.log(input);




//정리하면 함수는 변수, 매개변수, 리턴값으로 사용할수있다
//이걸 first - class - citizen(object) 라고 한다.



/***********************
 * 
 * 콜백
 * 
 * ********************** */

//콜백은 어떠한 함수가 수신하는 인자가 함수인 경우를 콜백이라고 한다.


//  처리의 위임
// 값으로 사용될 수 있는 특성을 이용하면 함수의 인자로 함수로 전달할 수 있다.
//  값으로 전달된 함수는 호출될 수 있기 때문에 이를 이용하면 
//  함수의 동작을 완전히 바꿀 수 있다. 
//  인자로 전달된 함수 sortNumber의 구현에 따라서 
//  sort의 동작방법이 완전히 바뀌게 된다.
// //
//
// function sortNumber(a,b){
//     console.log(a,b)
//     // 위의 예제와 비교해서 a와 b의 순서를 바꾸면 정렬순서가 반대가 된다.
//     if(a>b){
//      return 1;
//     }
//     //뒤에가 큰 경우
//     else if(a<b){
//         return -1;
//     }
//     //같은 경우
//     else{
//         return
//     }
// }
//위에 놈을 간소화 하면 아래처럼 된다. 앞에 놈이 크면 양수니까 true ..
// function sortNumber(a,b){
//     // 위의 예제와 비교해서 a와 b의 순서를 바꾸면 정렬순서가 반대가 된다.
// 역순은 b-a
//     return a-b;
// }
//
// var numbers  = [20, 10, 9,8,7,6,5,4,3,2,1];
// console.log(numbers);

//// 이렇게 뒤에 . 이 붙는 놈은 객체라고 한다 (numbers) 즉 배열 객체이다. 배열 객체에는 sort 함수가 정의 되어 있다
//// 이런 맥락에서 sort를 객체에 속해 있기 때문에 함수가 아니라 메소드라고 한다. 이런걸 내장객체라고 한다.
//// 우리가 만드는건 사용자 정의 객체, 사용자 정의 함수라고 한다.
//// 여기서 순서가 제대로 안나오는건 숫자로 인식안하고 텍스트로 인식해서 그렇다.
// console.log(numbers.sort());

////sort라는 함수는 내부적으로 값을 정렬할 때
// sortNumber라는 변수가 가리키는
// 이 함수의 내용을 실행함을 통해서 값들이 어떤 것이 높은건지를
////판별하게 된다
//sortnumber가 콜백함수다. 즉 sortnumber를 수신받는 메소드 sort가  콜백함수의 내용을 인자로 전달 받아서 내부적으로 호출하는 것을 통해서
//이 sort가 동작하는 기본적인 동작 방법을 변경할수 있는 것이다. 즉, 값으로 함수를 사용할 수 있기 때문에
//이 오리지날 함수 (sort) 의 동작방법을 값을 전달하는 것을 통해서 완전히 바꿀 수 있다는 것이 콜백이다. 콜백이 가능한것은 자바스크립트 함수가
//값이기 때문에 가능한거다.
//
// console.log(numbers.sort(sortNumber));
//


//콜백은 비동기처리에서도 유용하게 사용된다.
// 시간이 오래걸리는 작업이 있을 때 이 작업이 완료된 후에
// 처리해야 할 일을 콜백으로 지정하면 해당 작업이 끝났을 때
// 미리 등록한 작업을 실행하도록 할 수 있다.
// 다음 코드는 일반적인 환경에서는 작동하지 않고
// 서버 환경에서만 동작한다. 동영상을 참고한다.

//예를 들면 어떤 홈페이지를 운영하는데 10000명 정도의 구독자가 있다
// 이 홈페이지는 글을 작성하면 10000명 구독자에게 메일을 보낸다.
// 결국 사용자가 글을 작성하고 10000명에게 이메일을 보낼때
// 한명당 1초가 걸리면 2시간 이상이 걸리는거다.

// 글작성 -> 이메일발송 -> 작성완료 : 3시간
// 위와 같은 순서가 동기적이라고 표현. 순서대로 했다.
//근데 이메일 발송을 예약하고 작성완료를 해버리면 이건 순식간에 끝난다.
// 즉, 내부적으로 사용자에게 노출되지 않은 프로그램이 반복적으로 동작하면서
// 발송예약이 들어와있다면 3시간동안 백그라운드에서 동작하면 되는거다.
// 즉 to do 같은거다.이걸 비동기 처리라고 한다
// ajax (asynchronous -비동기, javascript and xml)에서 사용한다.
// ajax 를 이용하면 홈페이지에서 게시판으로 이동하는 형태로 url이 변경되는 형태말고
// 내 컨텐츠, 쪽지 확인 같은거 눌렀을 때 url 위치를 안 옮겨도 그냥
//홈페이지 위에 해당 내용들이 표시 되는것들이 가능해진다.
// 이런걸 비동기적 처리라고 하는데 예를 들어서 서버에서 쪽지 버튼 눌렀을 때에
// 정보들을 빨리 전달해주지 않더라고 나는 현재의 페이지에서 스크롤도 할 수 있고 다른 작업을
//할 수 있다.





/***********************
 *
 * 클로저
 *
 * ********************** */

// 클로저(closure)는 내부함수가 외부함수의 맥락(context)에 접근할 수 있는 것을 가르킨다.
// 클로저는 자바스크립트를 이용한 고난이도의 테크닉을 구사하는데
// 필수적인 개념으로 활용된다.
// 사용하는 예는 어떤 특정 함수 안에서만 사용되는 함수가 있는경우 함수밖에다가 inner를 만들면
//응집성이 떨어지기 때문에 이럴때 사용
//
// function outter(){
//     function inner(){
//         var title = 'coding everybody';
//         console.log(title);
//     }
//     inner();
// }
// outter();


//위의 예제에서 함수 outter의 내부에는 함수 inner가 정의 되어 있다.
// 함수 inner를 내부 함수라고 한다.
// 다르게 해석하면 outter 라는 외부함수에 var inner = funtion(){} 가 있는거다.
// 즉, innner라는 변수에 funtion이 값으로 존재하는거다

// 내부함수는 외부함수의 지역변수에 접근할 수 있다.
// 아래의 예제를 보자. 결과는 coding everybody이다.
//
//
// function outter(){
//     var title = 'coding everybody';
//     function inner(){
//         console.log(title);
//     }
//     inner();
// }
// outter();






// 클로저(closure)는 내부함수와 밀접한 관계를 가지고 있는 주제다.
// 내부함수는 외부함수의 지역변수에 접근 할 수 있는데
// 외부함수의 실행이 끝나서 외부함수가 소멸된 이후에도
// 내부함수가 외부함수의 변수에 접근 할 수 있다.
// 이러한 메커니즘을 클로저라고 한다.
// 아래 예제는 이전의 예제를 조금 변형한 것이다.
// 결과는 경고창으로 coding everybody를 출력할 것이다.


//
// function outter(){
//     var title = 'coding everybody';
//     //return 을 하는 순간 outter는 생이 끝나는거다.
//     return function(){
//         console.log(title);
//     }
// }
//
// // 내부함수 function 이 inner에 담긴다
// //outter 생이 끝났는데도 외부함수에 접근이 가능하고 그 안에 변수도 접근 가능한거다
// inner = outter();
// inner();

// 예제의 실행순서를 주의깊게 살펴보자. 7행에서 함수 outter를 호출하고 있다.
// 그 결과가 변수 inner에 담긴다. 그 결과는 이름이 없는 함수다.
// 실행이 8행으로 넘어오면 outter 함수는 실행이 끝났기 때문에
// 이 함수의 지역변수는 소멸되는 것이 자연스럽다.
// 하지만 8행에서 함수 inner를 실행했을 때 coding everybody가
// 출력된 것은 외부함수의 지역변수 title이 소멸되지 않았다는 것을 의미한다.
// 클로저란 내부함수가 외부함수의 지역변수에 접근 할 수 있고,
// 외부함수는 외부함수의 지역변수를 사용하는 내부함수가 소멸될 때까지
// 소멸되지 않는 특성을 의미한다.
//
// 조금 더 복잡한 아래 예제를 살펴보자.
// 아래 예제는 클로저를 이용해서 영화의 제목을 저장하고 있는 객체를 정의하고 있다.
// 실행결과는 Ghost in the shell -> Matrix -> 공각기동대 -> Matrix 이다.
//title은 외부함수에 담겨있는 지역 변수다
//아래처럼 클로저를 이용하게 되면 title이라는 지역변수는 반드시 get_title, set_title 이라는
//메소드로만 접근이 가능하기 때문에 title이라는 변수를 아무나 수정을 할 수 없기 때문에
//외부에서 다른사람이 title을 어떤 맥락으로 사용하던지 간에 이 맥락에 영향을 주지않는다.
//즉, 데이터가 안전하게 관리되고 수정된다. private하게 관리!

//
// function factory_movie(title){
//     //객체를 return한다. {} 안에 있는게 객체
//     return {
//         //get_title, set_title은 속성이고, function이 정의 되었기 때문
//         // 이 두개의 속성은 메소드가 된다
//         //즉 factory_movie안에 정의가 된 메소드 이기 때문에 내부함수이다. 다만 내부함수의 소속이 객체일 뿐이다.
//         get_title : function (){
//             return title;
//         },
//         set_title : function(_title){
//             if(typeof _title === 'string'){
//                 title = _title;
//             }
//             else{
//                 console.log(typeof _title)
//                 console.log("문자열만 가능합니다!!");
//             }
//         }
//     }
// }
//
// //ghost, matrix는 위에서 return한 {} 안의 객체를 의미한다.
// ghost = factory_movie('Ghost in the shell');
// matrix = factory_movie('Matrix');
//
// console.log(ghost.get_title());
// console.log(matrix.get_title());
//
// ghost.set_title(1);
// ghost.set_title('공각기동대');
//
// console.log(ghost.get_title());
// console.log(matrix.get_title());

//
//
// 위의 예제를 통해서 알 수 있는 것들을 정리해보면 아래와 같다.
//
// 1. 클로저는 객체의 메소드에서도 사용할 수 있다.
// 위의 예제는 함수의 리턴값으로 객체를 반환하고 있다.
// 이 객체는 메소드 get_title과 set_title을 가지고 있다.
// 이 메소드들은 외부함수인 factory_movie의 인자값으로
// 전달된 지역변수 title을 사용하고 있다.
//
// 2. 동일한 외부함수 안에서 만들어진 내부함수나
// 메소드는 외부함수의 지역변수를 공유한다.
// 17행에서 실행된 set_title은 외부함수 factory_movie의
// 지역변수 title의 값을 '공각기동대'로 변경했다.
// 19행에서 ghost.get_title();의 값이 '공각기동대'인 것은
// set_title와 get_title 함수가 title의 값을 공유하고 있다는 의미다.
//
// 3. 그런데 똑같은 외부함수 factory_movie를
// 공유하고 있는 ghost와 matrix의 get_title의 결과는 서로 각각 다르다.
// 그것은 외부함수가 실행될 때마다 새로운 지역변수를 포함하는 클로저가
// 생성되기 때문에 ghost와 matrix는 서로 완전히 독립된 객체가 된다.
//
// 4. factory_movie의 지역변수 title은 2행에서 정의된
// 객체의 메소드에서만 접근 할 수 있는 값이다.
// 이 말은 title의 값을 읽고 수정 할 수 있는 것은
// factory_movie 메소드를 통해서 만들어진 객체 뿐이라는 의미다.
// JavaScript는 기본적으로 Private한 속성을 지원하지 않는데,
// 클로저의 이러한 특성을 이용해서 Private한 속성을 사용할 수 있게된다.
//
//     참고 Private 속성은 객체의 외부에서는 접근 할 수 없는
//     외부에 감춰진 속성이나 메소드를 의미한다.
//     이를 통해서 객체의 내부에서만 사용해야 하는 값이
//     노출됨으로서 생길 수 있는 오류를 줄일 수 있다.
//     자바와 같은 언어에서는 이러한 특성을 언어 문법 차원에서 지원하고 있다.
//     아래의 예제는 클로저와 관련해서 자주 언급되는 예제다.
//
//
//
// var arr = []
// for(var i = 0; i < 5; i++){
//     //여기 i는 외부 변수의 i가 아니다. 아래 function 함수의 외부 변수가 아니다.
//     // i는 전역변수 i를 바라보고 있는거다.
//     arr[i] = function(){
//         return i;
//     }
// }
// for(var index in arr) {
//     console.log(arr[index]());
// }
//
// //함수가 함수 외부의 컨텍스트에 접근할 수 있을 것으로 기대하겠지만 위의 결과는 5만 나온다
// // 위의 코드는 아래와 같이 변경해야 한다.
//
// var arr = []
// for(var i = 0; i < 5; i++){
//     //id는 지역변수다
//     arr[i] = function(id) {
//         //즉, 아래의 내부함수는 외부함수의 지역변수 id를 return
//         return function(){
//             return id;
//         }
//     //    아래 () 는 즉석에서 바로 호출하겠다는 뜻,
//     }(i);
// }
// for(var index in arr) {
//     console.log(arr[index]());
// }






/***********************
 *
 * arguments
 *
 * ********************** */
//함수에는 arguments라는 변수에 담긴 숨겨진 유사 배열이 있다.(배열은 아니고 배열과
//비슷해서 유사배열이랑 칭함)
// 이 배열에는 함수를 호출할 때 입력한 인자가 담겨있다.
// 아래 예제를 보자. 결과는 10이다.
//arguments를 다른 언어로 바꾸면 안된다

//// 일단 용어 정리를 하면 여기서 a는 parameter(매개변수 ), 1은 인자
//function sum(a){
// }
// sum(1)


// //여기서 sum의 매개변수가 정의되어있지 않다.
// function sum(){
//     var i, _sum = 0;
//     //arguments 안에는 아래 사용자가 정의한 인자가 담겨져 있다
//     console.log("arguments[1]"+arguments[1])
//     for(i = 0; i < arguments.length; i++){
//         console.log(i+' : '+arguments[i]+'\n');
//         _sum += arguments[i];
//     }
//     return _sum;
// }
//
// //자바스크립트는 관대하기 때문에 매개변수가 정해져있지 않아도 아무리 많은 인자를 쓰더라도 에러가 안난다.
// console.log('result : ' + sum(1,2,3,4));
//

//함수 sum은 인자로 전달된 값을 모두 더해서 리턴하는 함수다.
// 그런데 1행처럼 함수 sum은 인자에 대한 정의가 없다.
// 하지만 마지막 라인에서는 4개의 인자를 함수 sum으로 전달하고 있다.
// 함수의 정의부분에서 인자에 대한 구현이 없음에도 인자를 전달 할 수 있는 것은
// 왜 그럴까? 그것은 arguments라는 특수한 배열이 있기 때문이다.
//
// arguments는 함수안에서 사용할 수 있도록 그 이름이나 특성이
// 약속되어 있는 일종의 배열이다.
// arguments[0]은 함수로 전달된 첫번째 인자를 알아낼 수 있다.
// 또 arguments.length를 이용해서 함수로 전달된 인자의 개수를 알아낼 수도 있다.
// 이러한 특성에 반복문을 결합하면 함수로 전달된 인자의 값을 순차적으로
// 가져올 수 있다. 그 값을 더해서 리턴하면 인자로 전달된 값에 대한
// 총합을 구하는 함수를 만들 수 있다.
//
// arguments는 사실 배열은 아니다. 실제로는 arguments 객체의 인스턴스다.



/*매개변수의 수 */
//매개변수와 관련된 두가지 수가 있다.
// 하나는 함수.length, 다른 하나는 arguments.length이다.
// arguments.length는 함수로 전달된 실제 인자의 수를 의미하고,
// 함수.length는 함수에 정의된 인자의 수를 의미한다. 아래의 코드를 보자.

//
// function zero(){
//     console.log(
//         'zero.length', zero.length,
//         'arguments', arguments.length
//     );
// }
// function one(arg1){
//     console.log(
//         'one.length', one.length,
//         'arguments', arguments.length
//     );
//     console.log(
//         'arguments0', arguments[0]
//     );
//     console.log(
//         'arguments1', arguments[1]
//     );
// }
// function two(arg1, arg2){
//     console.log(
//         'two.length', two.length,
//         'arguments', arguments.length
//     );
// }
// zero(); // zero.length 0 arguments 0
// one('val1', 'val2');  // one.length 1 arguments 2
// two('val1');  // two.length 2 arguments 1








/***********************
 *
 * 함수의 호출
 *
 * ********************** */

//함수에 대한 기본 수업에서 함수를 호출하는 방법을 알아봤다.
// 아래는 함수를 호출하는 가장 기본적인 방법이다.
//
// function func(){
// }
// func();

// JavaScript는 함수를 호출하는 특별한 방법을 제공한다.
// 본 토픽의 시작에서 함수를 객체라고 했다.
// 객체는 속성을 가지고 있다. 속성에 값이 저장되어있다면 우리는 그것을 그냥 속성(property)이라고 부른다
// 근데 그 속성에 함수가 들어있다면 그걸 메소드라고 부른다.

// 위의 예제에서 함수 func는 Function이라는 객체의 인스턴스다.
// 즉 function은 빵틀 , func는 붕어빵

// 따라서 func는 객체 Function이 가지고 있는 메소드들을 상속하고 있다.
// 아래 예제에서 sum 하고 . 찍어보면 쭉 나오는 함수들이 내장 함수다.

// 지금 이야기하려는 메소드는 Function.apply과 Function.call이다.(내장함수)
// 이 메소드들을 이용해서 함수를 호출해보자. 결과는 3이다.
//
// function sum(arg1, arg2){
//     return arg1+arg2;
// }
// console.log(sum.apply(null, [1,2]))
//
// //
// // 함수 sum은 Function 객체의 인스턴스다.
// // 그렇기 때문에 객체 Function 의 메소드 apply를 호출 할 수 있다.
// // apply 메소드는 두개의 인자를 가질 수 있는데,
// // 첫번째 인자는 함수(sum)가 실행될 맥락이다.
// // 맥락의 의미는 다음 예제를 통해서 살펴보자. 두번째 인자는 배열인데,
// // 이 배열의 담겨있는 원소가 함수(sum)의 인자로 순차적으로 대입된다.
// // Function.call은 사용법이 거의 비슷하다 여기서는 언급하지 않는다.
// //
// //     좀 더 흥미로운 예제를 살펴보자. 결과는 6과 185이다.
//
//
// o1 = {val1:1, val2:2, val3:3}
// o2 = {v1:10, v2:50, v3:100, v4:25}
// function sum(){
//     var _sum = 0;
//     //여기서 this가 뭔지 모른다. sum을 호출할 때 알수 있다.
//     // 아래에서 호출하면
//     // var this = o1; 이게 암묵적으로 실행
//     for(name in this){
//         _sum += this[name];
//     }
//     return _sum;
// }
//
// //함수를 호출 할 때는 sum() 이렇게도 호출하지만 apply 메소드를 이용할 수도 있다.
// //o1 이 위의 함수의 this가 된다
// console.log(sum.apply(o1)) // 6
//
// console.log(sum.apply(o2)) // 185
// //


// 예제가 복잡해보이지만 한나씩 분해해서 생각해보면 어렵지 않다.
//
//     우선 두개의 객체를 만들었다. o1는 3개의 속성을 가지고 있다.
//     각각의 이름은 val1, val2,val3이다.
//     o2는 4개의 속성을 가지고 있고 o1과는 다른 속성 이름을 가지고 있고
//     속성의 수도 다르다.
//
//     그 다음엔 함수 sum을 만들었다.
//     이 함수는 객체의 속성을 열거할 때 사용하는 for in 문을 이용해서
//     객체 자신(this)의 값을 열거한 후에 각 속성의 값을 지역변수 _sum에
//     저장한 후에 이를 리턴하고 있다.
//
//     객체 Function의 메소드 apply의 첫번째 인자는 함수가 실행될 맥락이다.
//     이렇게 생각하자.
//     sum.apply(o1)은 함수 sum을 객체 o1의 메소드로 만들고
//     sum을 호출한 후에 sum을 삭제한다. 아래와 비슷하다.
// o1.sum = sum;
// alert(o1.sum());
// delete o1.sum();
//     (실행결과가 조금 다를 것이다. 그것은 함수
//     for in문으로 객체 o1의 값을 열거할 때 함수 sum도 포함되기 때문이다.)
//그걸 방지하려면 아래처럼 한다.

//
// o1 = {val1:1, val2:2, val3:3}
// o2 = {v1:10, v2:50, v3:100, v4:25}
// function sum(){
//     var _sum = 0;
//     //여기서 this가 뭔지 모른다. sum을 호출할 때 알수 있다.
//     // 아래에서 호출하면
//     // var this = o1; 이게 암묵적으로 실행
//     for(name in this){
//         if(typeof this[name] !== "function"){
//             _sum += this[name];
//         }
//
//     }
//     return _sum;
// }
//
// //함수를 호출 할 때는 sum() 이렇게도 호출하지만 apply 메소드를 이용할 수도 있다.
// //o1 이 위의 함수의 this가 된다
// console.log(sum.apply(o1)) // 6
//
// console.log(sum.apply(o2)) // 185
// //





/***********************
 *
 * 객체지향
 *
 * ********************** */


//객체지향 프로그래밍은 크고 견고한 프로그램을 만들기 위한 노력의 산물이다.
// 객체지향이라는 큰 흐름은 현대적 프로그래밍 언어들을 지배하고 있는
// 가장 중요한 맥락이라고 할 수 있다.
// 하지만 자바스크립트의 객체지향은 다른 언어들의 객체지향과 사뭇 다르다.
// 특히 Java나 C++과 같은 주류 객체지향 언어에 익숙한 독자라면 극심한
// 혼란을 경험할 수도 있다.
// 바로 이러한 특성 때문에 웃으면서 들어갔다가 울면서 나오게 된다.
//
// 하지만 최소한 주류가 된 언어라면 그 언어가 추구한 나름대로의 지향점이
// 있을 것이다. 그 지향점에 대해서 이해하고 언어를 대한다면 훨씬
// 더 즐겁게 언어를 음미할 수 있을 것이다.
// 특히 모든 처리의 중심에 함수를 두는 자바스크립트를 공부하다 보면
// 객체지향을 이렇게도 추구 할수도 있는 거구나 하는 놀라움을 느낄 수 있다.
//
// 객체지향 프로그래밍(Object-Oriented Programming)은
// 좀 더 나은 프로그램을 만들기 위한 프로그래밍 패러다임으로 로직을
// 상태(state)와 행위(behave)로 이루어진 객체로 만드는 것이다.
// 이 객체들을 마치 레고 블럭처럼 조립해서 하나의 프로그램을 만드는 것이
// 객체지향 프로그래밍이라고 할 수 있다.
// 다시 말해서 객체지향 프로그래밍은 객체를 만드는 것이다.
// 따라서 객체지향 프로그래밍의 시작은 객체란 무엇인가를 이해하는 것이라고
// 할 수 있다. 말이 어렵게 느껴지지 않는가? 그것은 아직 객체 지향에 대한
// 체험이 없기 때문이다. 본 수업에서는 객체 지향에 대해서 코드 없이
// 이야기 할 수 있는 것들에 대해서만 이야기 할 생각이다.
// 객체 지향에 대한 오리엔테이션이라고 생각하고 가벼운 마음으로
// 나머지 내용을 읽어보자.
//
//     객체지향 프로그래밍을 학습하는데 장애 중의 하나는 번역이다.
//     Object를 번역한 객체는 현실에서는 거의 쓰지 않는 말이고,
//     머랄까 철학적인 느낌을 자아낸다.
//     그래서 객체지향 프로그래밍을 처음 접하는 입문자들은
//     객체지향 프로그래밍을 철학적인 탐구의 대상으로 파악하는 경향을 보이는데,
//     필자의 생각에 이것은 공부를 어렵게 할 뿐 도움이 되지 않는다.
//     쉽게 생각하자.
//
//     객체는 변수와 메소드를 그룹핑한 것이다.

//     /*문법과 설계*/
// 객체지향 프로그래밍 교육은 크게 두 가지로 구분된다.
//
//     /*문법*/
// 하나는 객체지향을 편하게 할 수 있도록 언어가 제공하는 기능을 익히는 것이다.
// 이러한 기능들은 if, for문처럼 문법적인 구성을 가지고 있다.
// 이 문법을 이해하고, 숙지해야 객체를 만들 수 있다.
// 객체를 만드는 법에 대한 학습이라고 할 수 있다.
// 우리 수업은 여기에 초점이 맞춰져 있다.

// /*설계*/
//두번째는 좋은 객체를 만드는 법이다.
// 이것을 다른 말로는 설계를 잘하는 법이라고 할 수 있다.
// 좋은 설계는 현실을 잘 반영해야 한다.
// 현실은 복잡하다. 하지만 그 복잡함 전체가 필요한 것은 아니다.
// 아래의 그림을 보자.


//위의 그림은 런던의 지도다. 여러분이 지하철을 이용한다면
// 어떤 지도를 선호할까? 오른쪽 하단의 지도를 선호할 것이다.
// 왼쪽 상단의 지도는 현실의 복잡함을 나타낸다.
// 오른쪽 하단의 지도는 지하철 탑승자의 관심사만을 반영하고 있다.
// 역 간의 거리나 실제 위치와 같은 요소들은 모두 배제하고 있다.
// 복잡함 속에서 필요한 관점만을 추출하는 행위를 추상화라고 한다.
//
// 지하철 노선도가 디자인의 추상화라고 한다면 프로그램을 만든다는 것은
// 소프트웨어의 추상화라고 할 수 있다. 객체 지향 프로그래밍은 좀 더
// 현실을 잘 반영하기 위한 노력의 산물이다.
// 이것은 단순히 객체 지향의 문법을 이용해서 객체를 만든다고
// 달성되는 것이 아니다. 고도의 추상화 능력이 필요하다.
// 좋은 설계는 문법을 배우는 것보다 훨씬 어려운 일이다.
// 심지어 이것은 지식을 넘어서 지혜의 영역이다.
// 좋은 설계를 위한 조언들은 많지만 이러한 조언들은 조언자의 입을 떠나는
// 순간 생명력을 잃어버린다. 지식은 전수되지만 지혜는 전수되지 않기 때문이다.
// 스스로 경험하고 깨우쳐서 자기화시켜야 한다.
// 필자도 그 긴 여정을 따라가고 있는 견습생에 불과하다.
//
// 객체지향의 설계 원칙이나 객체 지향의 철학적인 의미는 대단히 중요하다.
// 하지만 이러한 것들을 지금 언급한다면 여러분은 미궁 속에 빠지게 될 것이다.
// 그래서 필자가 제안하는 것은 일단은 지식부터 익히자는 것이다.
// 언어가 지원하는 객체지향 문법을 배우고,
// 이것들이 어떻게 동작하는지를 충분히 이해한 다음에 비로소 설계 원칙도
// 이야기할 수 있고, 객체와 사물의 비유도 시도해 볼 수 있을 것이다.
// 여기서는 몇 가지 객체지향이 추구하는 지향점을 가볍게 이야기하고
// 다음 토픽부터 구체적인 문법을 알아볼 것이다.




//프로그래밍은 정신적인 활동이다. 정신적인 것은 실체가 없고,
// 무한하고, 유연하다. 이러한 특성은 정신이 가진 장점이면서
// 소프트웨어의 극치다. 하지만 정신의 이러한 특성은 때로
// 오해나 모순 같은 문제점을 유발한다.
// 소프트웨어도 이러한 문제점을 그대로 상속받는다.
// 이러한 문제점을 극복하기 위한 노력 중의 하나가 부품화라고 할 수 있다.
// 객체 지향과 부품화를 동일시 할 수는 없지만 부품화라고 하는 소프트웨어의
// 큰 흐름은 객체 지향이 만들어지는데 지대한 공헌을 했다고 할 수 있다.
// 하드웨어에서 이루어지는 부품화의 예를 보자.
//
// 아래의 컴퓨터는 초창기의 컴퓨터다.
//
//
// 본체와 모니터와 키보드가 하나로 단일화되어 있다.
// 이것의 문제점은 분명하다. 모니터가 고장 나면 컴퓨터를 바꿔야 한다.
// 키보드가 고장 나도 컴퓨터를 교체해야 한다.
//
//
// 그래서 위와 같이 모니터와 본체와 컴퓨터를 분리했다.
// 다시 말해서 부품화 시킨 것이다. 기능들을 부품화 시킨 덕분에
// 소비자들은 더 좋은 키보드나 저렴한 모니터를 선택할 수 있게 되었다.
// 또 문제가 생겼을 때 그 문제가 어디에서 발생한 것인지 파악하고
// 해결하기가 훨씬 쉬워진다.
//
// 위의 그림에서 모니터와 키보드 그리고 본체를 분리하는 기준은 무엇일까?
// 그 기준을 세우는 것이 추상화일 것이다.
// 위 제품의 기획자는 컴퓨터를 입력과 출력 그리고 연산 & 저장으로 분류하고 있다.
// 이 분류에 따라서 부품들을 모으고 분리해서 모니터, 키보드, 본체와
// 마우스라는 개별적인 완제품을 만들고 있다.
// 이 완제품들을 부품으로 조합하면 컴퓨터라는 하나의 완제품이 만들어진다.
//
// 물론 정해진 답이 있는 것은 아니다.
// 아래 컴퓨터는 저장 장치를 부품화시키고 있다.
//
//
// 또 아래 장치는 현시점에서 최신 데스크탑이다.
// 그런데 부품화를 제거하고 있다.
// 기술이 경량화되면서 컴퓨터는 더욱 작아지게 되었고,
// 그 결과 컴퓨터를 부품화하는 것의
// 매력이 반감되고 있기 때문이 아닐까?
// 부품화가 중요한 것임에는 분명하지만 그 보다 중요한 것은 적절함이다.
// 그래서 설계가 어려운 것이다.
//
//
//
//
// 객체 지향은 부품화의 정점이라고 할 수 있다.
// 하지만 우리는 아직 객체 지향을 배우지 않았다.
// 그래서 우리가 배운 것 중에서 부품화의 특성을 보여줄 수 있는 기능을
// 생각해보면 좋을 것 같다.
// 메소드는 부품화의 예라고 할 수 있다.
// 메소드를 사용하는 기본 취지는 연관되어 있는 로직들을 결합해서
// 메소드라는 완제품을 만드는 것이다.
// 그리고 이 메소드들을 부품으로 해서 하나의 완제품인
// 독립된 프로그램을 만드는 것이다.
// 메소드를 사용하면 코드의 양을 극적으로 줄일 수 있고,
// 메소드 별로 기능이 분류되어 있기 때문에 필요한
// 코드를 찾기도 쉽고 문제의 진단도 빨라진다.
//
// 그런데 프로그램이 커지면 엄청나게 많은 메소드들이 생겨나게 된다.
// 메소드와 변수를 관리하는 것은 점점 어려운 일이 되기 시작한다.
// 급기야는 메소드가 없을 때와 같은 상황에 봉착하게 된다.
// 메소드는 프로그래밍의 역사에서 중요한 도약이었지만,
// 이 도약이 성숙하면서 새로운 도약지점이 보이기 시작한 것이다.
//
// 그 도약 중의 하나가 객체 지향 프로그래밍이다.
// 이것의 핵심은 연관된 메소드와 그 메소드가 사용하는 변수들을 분류하고
// 그룹핑하는 것이다.
// 바로 그렇게 그룹핑 한 대상이 객체(Object)다.
// 비유하자면 파일과 디렉토리가 있을 때 메소드나 변수가 파일이라면
// 이 파일을 그룹핑하는 디렉토리가 객체라고 할 수 있다.
// 이를 통해서 더 큰 단위의 부품을 만들 수 있게 되었다.
// 객체를 만드는 법에 대해서 호기심이 생기지 않는가?
// 이런 호기심을 유발시키는 것이 이번 토픽의 목적이다.
// 객체를 만드는 법은 다음 토픽에서 알아보고 지금은 부품화에 대해서
// 조금 더 생각해보자.
//
// 은닉화, 캡슐화
// 그런데 부품화라고 하는 목표는 단순히 동일한 기능을 하는 메소드와 변수를
// 그룹핑한다고 달성되는 것은 아니다.
// 제대로된 부품이라면 그것이 어떻게 만들어졌는지 모르는 사람도
// 그 부품을 사용하는 방법만 알면 쓸 수 있어야 한다.
// 이를테면 모니터가 어떻게 동작하는지 몰라도 컴퓨터와 모니터를
// 연결하는 방법만 알면 화면을 표시 할 수 있는 것과 같은 이치다.
// 즉 내부의 동작 방법을 단단한 케이스 안으로 숨기고
// 사용자에게는 그 부품의 사용방법만을 노출하고 있는 것이다.
// 이러한 컨셉을 정보의 은닉화(Information Hiding),
// 또는 캡슐화(Encapsulation)라고 부른다.
// 자연스럽게 사용자에게는
// 그 부품을 사용하는 방법이 중요한 것이 된다.
//
// 인터페이스
// 잘 만들어진 부품이라면 부품과 부품을 서로 교환 할 수 있어야 한다.
// 예를들어보자. 집에 있는 컴퓨터에 A사의 모니터를
// 연결하다가 B사의 모니터를 연결 할 수 있다.
// 또 집에 있던 모니터에 A사의 컴퓨터를 연결해서
// 사용하다가 새로운 컴퓨터를 구입하면서 B사의 컴퓨터를 연결 할 수 있다.
// 모니터와 컴퓨터는 서로가 교환관계에 있는 것이다.
// 이것은 모니터와 컴퓨터를 연결하는 케이블의 규격이
// 표준화 되어 있기 때문에 가능한 일이다.
// 아래의 그림을 보자. 모니터와 컴퓨터를 연결하는 케이블인 HDMI를 보여준다.
//
//
//
// 컴퓨터와 모니터를 만드는 업체들은 위와 같은 케이블의 규격을 공유한다.
// 모니터 입장에서는 컴퓨터가,
// 컴퓨터 입장에서는 모니터가 어떤 식으로 만들어졌는지는 신경쓰지 않는다.
// 각각의 부품은 미리 정해진 약속에 따라서 신호를
// 입, 출력하고, 연결점의 모양을 표준에 맞게 만들면 된다.
// 이러한 연결점을 인터페이스(interface)라고 한다.
// 위의 그림을 보면 HDMI 케이블의 연결점은 특유의 생김새가 있다.
// 만약 HDMI 케이블을 랜선을 연결하는 구멍에 연결하려고 한다면 어떻게 될까?
// 동작하지 않을 뿐 아니라 연결 자체가 되지 않는다.
// 인터페이스란 이질적인 것들이 결합하는 것을 막아주는 역할도 하는 것이다.
// 즉 인터페이스는 부품들 간의 약속이다.
// 이러한 약속을 프로그래밍적으로는 어떻게 구현하는가도 살펴본다.
//
// 지금까지 객체를 부품으로 비유해서 설명 했다.
// 그런데 비유는 비유일 뿐이다.
// 비유는 의도한 유사점 뿐만 아니라 의도하지 않은 차이점까지도
// 전달될 가능성이 있기 때문이다.
// 비유의 함정이라고 할 수 있다.
// 소프트웨어는 하드웨어가 아니다.
// 하드웨어가 할 수 없는 것을 소프트웨어는 할 수 있다.
// 그 중의 하나가 복제와 상속이다.
// 이러한 개념을 구체적인 문법 없이 설명하는 것은 효용이 크지 않을 뿐만 아니라
// 자칫 흥미를 저해할 위험이 있기 때문에 여기서는 설명하지 않았다.
// 소프트웨어가 있기 이전부터 하드웨어가 이룩한 성취를 잘 수용하면서
// 동시에 소프트웨어 다운 소프트웨어를 만드는 것은 우리게게
// 주워진 숙제라고 할 수 있다.







/***********************
 *
 * 생성자와 new
 *
 * ********************** */
    //자바스크립트의 객체는 전통적이지 않다.
//객체란 서로 연관된 변수와 함수를 그룹핑한 그릇이라고 할 수 있다.
// 객체 내의 변수를 프로퍼티(property) 함수를 메소드(method)라고 부른다.
// 객체를 만들어보자.
    /**대괄호를 만들면 객체가 만들어진다*/
        //아래는 object(객체)를 빈상태로 만든거다.

//
//
// var person = {}
//
// //name은 속성, property라고 한다
// person.name = 'egoing';
// person.introduce = function(){
//     //this는 현재의 function가 속해있는 객체 즉, person변수가 담고있는 객체를 의미한다. 그리고 그 객체에 담겨있는
//     //name을 의미하기때문에 egoing을 가져온다
//     return 'My name is '+this.name;
// }
// console.log( person.introduce());
//
//
// // 객체를 만드는 과정에 분산되어 있다.
// // 객체를 정의 할 때 값을 셋팅하도록 코드를 바꿔보자.
//
// //위에 놈과 차이점은 객체 생성할때 동시에 객체 내 속성을 선언하는거다.
// var person = {
//     'name' : 'egoing',
//     'introduce' : function(){
//         return 'My name is '+this.name;
//     }
// }
// console.log(person.introduce());
//
// //만약 다른 사람의 이름을 담을 객체가 필요하다면
// // 객체의 정의를 반복해야 할 것이다.
// // 객체의 구조를 재활용할 수 있는 방법이 필요하다.
// // 이 때 사용하는 것이 생성자다.

//
//
// // 생성자(constructor)는 객체를 만드는 역할을 하는 함수다.
// // 자바스크립트에서 함수는 재사용 가능한 로직의 묶음이 아니라
// // 객체를 만드는 창조자라고 할 수 있다.
//
// function Person(){}
// var p = new Person();
// p.name = 'egoing';
// p.introduce = function(){
//     return 'My name is '+this.name;
// }
// console.log(p.introduce());

//
// // 함수를 호출할 때 new을 붙이면 새로운 객체를 만든 후에 이를 리턴한다.
// // 위의 코드는 새로운 객체를 변수 p에 담았다.
// // 여러사람을 위한 객체를 만든다면 아래와 같이 코드를 작성해야 할 것이다.
// //이 놈은 메소드가 중복 되어있다. => 코드의 가독성이 떨어진다. -> 이러한 중복을 해결하는게 생성자 new 다
// function Person(){}
//
// var p0 = Person();

/**이렇게 new 뒤에 호출 된 함수 Person은 생성자라고 한다. 뭘 생성? 객체의 생성자, 객체를 반환한다 **/
//     //자바스크립트에서는 클래스가 존재 하지 않는다.
// var p1 = new Person();
//
// console.log("Person() "+Person())
// //Person 을 담았기 때문에 p0는 그냥 undefined
// console.log("p0 "+p0)
// //new를 선언했기 때문에 객체
// console.log("p1 "+p1)
//
// p1.name = 'egoing';
// p1.introduce = function(){
//     return 'My name is '+this.name;
// }
// console.log(p1.introduce());
//
// var p2 = new Person();
// p2.name = 'leezche';
// p2.introduce = function(){
//     return 'My name is '+this.name;
// }
// console.log(p2.introduce());
//
// // 별로 개선된 것이 없다.







/**생성자는 객체에 대한 초기화(init)를 하는놈이다 **/

//
// function Person(name){
//     this.name = name;
//     this.introduce = function(){
//         return 'My name is '+this.name;
//     }
// }
// //아래는 에러 난다. 위에 Person 함수에서 this는 객체로 선언 될 때만 효과가 있기 때문
// // console.log(Person.introduce())
//
// var p1 = new Person('egoing');
// console.log(p1.introduce());
//
// var p2 = new Person('leezche');
// console.log(p2.introduce());
//

//생성자 내에서 이 객체의 프로퍼티를 정의하고 있다.
// 이러한 작업을 초기화라고 한다.
// 이를 통해서 코드의 재사용성이 대폭 높아졌다.
//
// 코드를 통해서 알 수 있듯이
/**생성자 함수는 일반함수와 구분하기 위해서 첫글자를대문자로 표시*/
 // 한다
//
//
// 자바스크립트 생성자의 특징
// 일반적인 객체지향 언어에서 생성자는 클래스의 소속이다.
// 하지만
/**자바스크립트에서 객체를 만드는 주체는 함수다.**/
// 함수에 new를 붙이는 것을 통해서 객체를 만들 수 있다는 점은
// 자바스크립트에서 함수의 위상을 암시하는 단서이면서
// 또 자바스크립트가 추구하는 자유로움을 보여주는 사례라고 할 수 있다.
//
//  .




/***********************
 *
 * 전역 객체
 *
 * ********************** */


//전역객체(Global object)는 특수한 객체다.
// 모든 객체는 이 전역객체의 프로퍼티다.
// function func(){
//     console.log('Hello?');
// }
// func();
// //이렇게 무언가 뒤에 . 을 붙인다는건 앞에놈 window가 객체이고 뒤에 나오는 놈이 속성
// //여기서는 속성이 func 니까 메소드
// //근데 window는 생략이 가능하다.
// //즉, 암시적으로 사용이 가능하다.
// //내부적으로는 window를 명시한것과 같다
// /**javascript 라는 프로그래밍 언어에서 모든 함수, 변수는 전역 함수, 전역 변수라 할지라도
//  * 사실을 윈도우라는 전역 객체의 property(속성) 이다. 그런점에서 javascript는
//  * 객체 지향 언어이다 . 객체라고 하는것에 모든것이 소속되어 있기 때문 **/
// global.func();

//func();와 window.func();는 모두 실행이 된다.
// 모든 전역변수와 함수는 사실 window 객체의 프로퍼티다.
// 객체를 명시하지 않으면 암시적으로 window의 프로퍼티로 간주된다.

// //여기서 o라는 변수는 func라는 메소드를 가지고 있다. 그리고 o는 객체이다.
// //그리고 o는 window라는 객체의 property다
// var o = {'func':function(){
//         console.log('Hello?');
//     }}
// o.func();
// global.o.func();
//

/**자바스크립트에서 모든 객체는 기본적으로 전역객체의 프로퍼티임을 알 수 있다.
**/

//전역객체 API
// ECMAScript에서는 전역객체의 API를 정의해두었다.
// 그 외의 API는 호스트 환경에서 필요에 따라서 추가로 정의하고 있다.
// 이를테면 웹브라우저 자바스크립트에서는 alert()이라는 전역객체의
// 메소드가 존재하지만 node.js에는 존재하지 않는다.
/** 또한 전역객체의 이름도 호스트환경에 따라서 다른데,
 웹브라우저에서 전역객체는 window이지만 node.js에서는 global이다.**/

/**아래 console 에서 node 라고 치고 테스트 해보면 된다 **/







/***********************
 *
 * 함수와 this
 *
 * ********************** */

//this는 함수 내에서 함수 호출 맥락(context)를 의미한다.
// 맥락이라는 것은 상황에 따라서 달라진다는 의미인데
// 즉 함수를 어떻게 호출하느냐에 따라서 this가 가리키는 대상이 달라진다는 뜻이다.
// 함수와 객체의 관계가 느슨한 자바스크립트에서
// this는 이 둘을 연결시켜주는 실질적인 연결점의 역할을 한다.

/**this는 javascript에서 함수안에서 사용되는 키워드이다  **/

//함수호출

// 함수를 호출했을 때 this는 무엇을 가르키는지 살펴보자.
// this는 전역객체인 window와 같다.
//여기서는 global이다 node 환경이니까

// function func(){
//     if(global === this){
//         console.log("global === this");
//     }
// }
// func();
//
//
// //객체의 소속인 메소드의 this는 그 객체를 가르킨다.
//
// var o = {
//     func : function(){
//         if(o === this){
//             console.log("o === this");
//         }
//     }
// }
// o.func();
//
//
// //아래 코드는 함수를 호출했을 때와 new를 이용해서 생성자를
// // 호출했을 때의 차이를 보여준다.
//
//
// var funcThis = null;
//
// function Func(){
//     //여기서 funcThis는 var가 없기 때문에 전역변수인 위에 funcThis를 가리킨다
//     //이 놈을 그냥 함수로서 호출하면 this가 global로 담긴다
//     // 왜냐면 기본적으로 함수, 변수는 global 객체의 property 이니깐!
//     //이 놈을 생성자로 호출하면 this는 생성되는 해당 객체를 가리킨다.
//     funcThis = this;
// }
// //일반 함수 호출
// var o1 = Func();
// if(funcThis === global){
//     console.log('window');
// }
//
// var o2 = new Func();
// if(funcThis === o2){
//     console.log('o2 ');
// }
//
// //생성자는 빈 객체를 만든다.
// // 그리고 이 객체내에서 this는 만들어진 객체를 가르킨다.
// // 이것은 매우 중요한 사실이다.
// // 생성자가 실행되기 전까지는 객체는 변수에도 할당될 수 없기 때문에
// // this가 아니면 객체에 대한 어떠한 작업을 할 수 없기 때문이다.
//
//
// function Func(){
//     console.log(o);
// }
// var o = new Func();
//
//
// /**apply, call**/
//
// //함수의 메소드인 apply, call을 이용하면 this의 값을 제어할 수 있다.
//     //즉, 함수가 객체이기 때문에 apply, call 이라는 내장함수를 메소드라고 하는거다.
//
// var o = {}
// var p = {}
//
// //이렇게 함수를 선언하는건 func라는 객체를 생성한다는 의미이다.
// // 이걸 함수리터럴 이라고 한다. (literal)
// //객체와 함수는 대등하다 왜냐? 함수도 객체이기 때문이다.
// //함수를 어떻게 호출을 했냐에 따라서 global에 소속이 되기도 하고 o, p 에 소속이 되기도 한다.
// /**즉, 그냥 호출하면 global의 노예(메소드), o가 호출하면 o의 노예(메소드), p가 호출하면 p의 노예(메소드) **/
//
// function func(){
//     switch(this){
//         case o:
//             console.log('o<br />');
//             break;
//         case p:
//             console.log('p<br />');
//             break;
//         case global:
//             console.log('global<br />');
//             break;
//     }
// }
// func();
// func.apply(o);
// func.apply(p);
// //
//
// //아래는 객체 리터럴
// var o ={}
//
// //아래는 배열 리터럴
// var a=[1,2,3]


//객체 - 주인 (maseter) , 메소드 - 노예 (종속관계이다!, slave)







/***********************
 *
 * 상속
 *
 * ********************** */

//객체는 연관된 로직들로 이루어진 작은 프로그램이라고 할 수 있다.
// 상속은 객체의 로직을 그대로 물려 받는 또 다른 객체를 만들 수 있는
// 기능을 의미한다. 단순히 물려받는 것이라면 의미가 없을 것이다.
// 기존의 로직을 수정하고 변경해서 파생된 새로운 객체를 만들 수 있게 해준다.
//
// 아래 코드는 이전 시간에 살펴본 코드다.

// function Person(name){
//     this.name = name;
//     this.introduce = function(){
//         return 'My name is '+this.name;
//     }
// }
// var p1 = new Person('egoing');
// console.log(p1.introduce()+"<br />");

//
//
// //위의 코드를 아래와 같이 바꿔보자.
// function Person(name){
//     this.name = name;
// }
// Person.prototype.name=null;
// Person.prototype.introduce = function(){
//     return 'My name is '+this.name;
// }
// var p1 = new Person('egoing');
// console.log(p1.introduce()+"<br />");
//
//
// //결과는 같다. 하지만 상속을 위한 기본적인 준비를 마쳤다. 이제 상속을 해보자.
//
//
//
// function Person(name){
//     //여기서 아래 this,name 도 필요없다.
//     // this.name = name;
// }
//
// //prototype도 propery다
// Person.prototype.name=null;
// Person.prototype.introduce = function(){
//     return 'My name is '+this.name;
// }
//
// function Programmer(name){
//     this.name = name;
// }
//
// //아래처럼 객체를 생성 시 javascript는 prototype 이라는 속성을 통해
// //생성자 함수가 가지고 있는 변수, 메소드 들을 확인한다.
// //확인한다
//
// Programmer.prototype = new Person();
//
// // Programmer를 객체화 하는 순간 javascript는 programmer 라는 생성자 함수가 가지고
// //있는 prototype 이라고 하는 property 값과 동일한 구조를 가지고 있는 동일한 객체를 만들어서
// // return 하여 p1에 준다
// //즉, p1은 Programmer 생성자의 property인 prototype 에 들어있는 객체와 같다
// // 그 객체는 new person 의 생성자로 만든 객체 이기 때문에 person 객체가 가지고 있는
// //introduce도 포함하는 것이다.
// var p1 = new Programmer('egoing');
// console.log(p1.introduce()+"<br />");
//
//
// //Programmer이라는 생성자를 만들었다.
// // 그리고 이 생성자의 prototype과 Person의 객체를 연결했더니
// // Programmer 객체도 메소드 introduce를 사용할 수 있게 되었다.
// //
// // Programmer가 Person의 기능을 상속하고 있는 것이다.
// // 단순히 똑같은 기능을 갖게 되는 것이라면 상속의 의의는 사라질 것이다.
// // 부모의 기능을 계승 발전할 수 있는 것이 상속의 가치다.



//
//
// function Person(name){
//     //아래는 필요가 읍서
//     // this.name = name;
// }
// Person.prototype.name=null;
// Person.prototype.introduce = function(){
//     return 'My name is '+this.name;
// }
//
// function Programmer(name){
//     this.name = name;
// }
// Programmer.prototype = new Person();
// //위에랑 차이는 아래3줄만 추가된거다.
// Programmer.prototype.coding = function(){
//     return "hello world";
// }
//
//
//
// function Designer(name){
//     this.name = name;
// }
// Designer.prototype = new Person();
// //위에랑 차이는 아래3줄만 추가된거다.
// Designer.prototype.desing = function(){
//     return "beautiful";
// }
//
// var p1 = new Programmer('egoing');
// console.log(p1.introduce()+"<br />");
// console.log(p1.coding()+"<br />");
//
// var p2 = new Designer('jake');
// console.log(p2.introduce()+"<br />");
// console.log(p2.desing()+"<br />");
//
//





/***********************
 *
 * prototype
 *
 * ********************** */

// 이번 수업은 상속수업의 연장선상에 있다.
// 상속의 구체적인 수단인 prototype에 대해서 자세히 알아보자.

//그럼 prototype이란 무엇인가?
// 한국어로는 원형정도로 번역되는 prototype은
// 말 그대로 객체의 원형이라고 할 수 있다.
// 함수는 객체다.
// 그러므로 생성자로 사용될 함수도 객체다.
// 객체는 프로퍼티를 가질 수 있는데
// prototype이라는 프로퍼티는 그 용도가 약속되어 있는 특수한 프로퍼티다.
// prototype에 저장된 속성들은 생성자를 통해서 객체가 만들어질 때
// 그 객체에 연결된다.
//아래와 같이 연결된 관계를 prototype chain 이라고 한다
// function Ultra(){}
// Ultra.prototype.ultraProp = true;
//
// function Super(){}
// Super.prototype = new Ultra();
//
// function Sub(){}
// Sub.prototype = new Super();
//
// var o = new Sub();
// console.log(o.ultraProp);


//생성자 Sub를 통해서 만들어진 객체 o가 Ultra의
// 프로퍼티 ultraProp에 접근 가능한 것은 prototype 체인으로
// Sub와 Ultra가 연결되어 있기 때문이다.
// 내부적으로는 아래와 같은 일이 일어난다.
//
// 객체 o에서 ultraProp를 찾는다.
// 없다면 Sub.prototype.ultraProp를 찾는다.
// 없다면 Super.prototype.ultraProp를 찾는다.
// 없다면 Ultra.prototype.ultraProp를 찾는다.
// prototype는 객체와 객체를 연결하는 체인의 역할을 하는 것이다.
// 이러한 관계를 prototype chain이라고 한다.


//
// //즉 1을 출력한다.
// function Ultra(){}
// Ultra.prototype.ultraProp = true;
//
// function Super(){}
//
// var t = new Ultra();
// // t.ultraProp = 4;
// Super.prototype = t
//
//
// function Sub(){}
// Sub.prototype = new Super();
// //위에서 Sub.prototype=super.prototype 이런식으로 사용하면 안된다. 생성자로 !
// // Sub.prototype.ultraProp= 2
//
// var o = new Sub();
// //1이 없으면 2를 출력 2도 정의 안하면 true 출력
// // o.ultraProp = 1
// console.log(o.ultraProp);
//








/***********************
 *
 * 표준 내장 객체의 확장
 *
 * ********************** */


//표준 내장 객체(Standard Built-in Object)는 자바스크립트가
// 기본적으로 가지고 있는 객체들을 의미한다.
// 내장 객체가 중요한 이유는 프로그래밍을 하는데
// 기본적으로 필요한 도구들이기 때문에다.
// 결국 프로그래밍이라는 것은 언어와 호스트 환경에 제공하는
// 기능들을 통해서 새로운 소프트웨어를 만들어내는 것이기 때문에
// 내장 객체에 대한 이해는 프로그래밍의 기본이라고 할 수 있다.
//
// 자바스크립트는 아래와 같은 내장 객체를 가지고 있다.
// Object
// Function
// Array
// String
// Boolean
// Number
// Math
// Date
// RegExp
// 이제 우리는 내장객체라는 하늘에서 뚝떨어진 이것들이
// 무엇인지를 보다 잘 이해할 수 있게 되었다.
// new가 무엇인지, 함수가 객체를 어떻게 만드는지도 알았다.
// 또 원한다면 자바스크립트의 내장 객체와 같은 것을 우리도
// 만들 수 있다는 것도 알았다.
// 이러한 지식을 바탕으로 좀 더 멋진 일을 해보자.


//배열을 확장해보자.
// 아래 코드는 배열에서 특정한 값을 랜덤하게 추출하는 코드다.
    //Array는 내장객체이다. 그래서 new 를 통해서 가져오는거
var arr = new Array('seoul','new york','ladarkh','pusan', 'Tsukuba');
function getRandomValueFromArray(haystack){
    //Math.random 은 0부터 1까지 소수 중 아무거나 가져온다
    //즉 x * Math.random() 을 하면 0이 최소, x 가 최대인, 그 사이에 있는 실수가 나온다
    //여기서는 0~4 사이의 실수가 나온다. 그리고 Math.floor를 통해 내림해서 정수를 만든다.
    var index = Math.floor(haystack.length*Math.random());
    return haystack[index];
}
console.log(getRandomValueFromArray(arr));

//이렇게 작성해도 된다! 잘못한 것이 아니다.
// 하지만 조금 더 세련된 방법은 이 함수를 배열 객체에 포함시키는 것이다.
// 그렇게하면 마치 배열에 내장된 메소드인 것처럼 위의 기능을 사용할 수 있다.
// 이런식으로 prototype을 확장함으로 통해서 모든 공통적으로 가지고 있어야하는
// 로직이 있다면 prototype 을 통해서 내장 객체를 확장할수있다.

Array.prototype.rand = function(){
    var index = Math.floor(this.length*Math.random());
    return this[index];
}
var arr = new Array('seoul','new york','ladarkh','pusan', 'Tsukuba');
console.log(arr.rand());






/***********************
 *
 * Object
 *
* ********************** */
    //Object 라고 하는 구체적인 이름을 갖고 있는 객체가 있다.
//Object 객체는 객체의 가장 기본적인 형태를 가지고 있는 객체이다.
// 다시 말해서 아무것도 상속받지 않는 순수한 객체다.
// 자바스크립트에서는 값을 저장하는 기본적인 단위로 Object를 사용한다.
var grades = {'egoing': 10, 'k8805': 6, 'sorialgi': 80};
//동시에 자바스크립트의 모든 객체는 Object 객체를 상속 받는데,
// 그런 이유로 모든 객체는 Object 객체의 프로퍼티를 가지고 있다.
// 즉, 모든 객체에 갖고 있으면 좋겠다는 기능을 추가하기 위해서는 Object에 prototype을 바꾸면 된다
//또한 Object 객체를 확장하면 모든 객체가 접근할 수 있는 API를 만들 수 있다.
// 아래는 Object 객체를 확장한 사례다.

